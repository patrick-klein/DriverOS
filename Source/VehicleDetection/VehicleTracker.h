/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    VehicleTracker.h                                                                  **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#ifndef VEHICLEDETECTION_VEHICLETRACKER_H_
#define VEHICLEDETECTION_VEHICLETRACKER_H_

#include <vector>
#include <opencv2/core/core.hpp>

#include "yolo.h"

class VehicleTracker
{
 public:
    VehicleTracker();
    void find_vehicles(const cv::Mat& frame, std::vector<std::vector<float>>* boxes);

 private:
    YOLODetector _yolo;
};

#endif  // VEHICLEDETECTION_VEHICLETRACKER_H_

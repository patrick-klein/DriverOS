##########################################################################################
##                                                                                      ##
##    Copyright (c) 2018, Patrick Klein                                                 ##
##                                                                                      ##
##    DriverOS                                                                          ##
##    Makefile                                                                          ##
##                                                                                      ##
##    Permission is hereby granted, free of charge, to any person obtaining a copy      ##
##    of this software and associated documentation files (the "Software"), to deal     ##
##    in the Software without restriction, including without limitation the rights      ##
##    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         ##
##    copies of the Software, and to permit persons to whom the Software is             ##
##    furnished to do so, subject to the following conditions:                          ##
##                                                                                      ##
##    The above copyright notice and this permission notice shall be included in all    ##
##    copies or substantial portions of the Software.                                   ##
##                                                                                      ##
##    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        ##
##    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          ##
##    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       ##
##    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            ##
##    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     ##
##    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     ##
##    SOFTWARE.                                                                         ##
##########################################################################################

####### Configuration

include Makefile.config

_LIB := LaneGuidance
_DFLAGS :=
INCLUDE :=
LIBRARY := `pkg-config --libs opencv`

ifdef USE_DISPLAY
	_DFLAGS += DISPLAY
endif

ifdef USE_GTEST
	_DFLAGS += GTEST_
	INCLUDE += -I $(GTEST_DIR)/include
	LIBRARY += -L $(GTEST_DIR) -lgtest
endif

ifdef USE_VIDEO
	_DFLAGS += VIDEO VIDEO_ID=$(VIDEO_ID)
endif

ifdef USE_LEPTON
	_LIB += LeptonI2C LeptonSPI
	_DFLAGS += LEPTON LEPTON_SPI=\"$(LEPTON_SPI)\" LEPTON_I2C=\"$(LEPTON_I2C)\"
endif

ifdef USE_OBD2
	_LIB += OBD2
	_DFLAGS += OBD2 OBD2_UART=\"$(OBD2_UART)\"
endif

ifdef USE_OLED
	_LIB += OLED
	_DFLAGS += OLED OLED_SPI=\"$(OLED_SPI)\" OLED_DC=\"$(OLED_DC)\" OLED_RST=\"$(OLED_RST)\"
endif

ifdef USE_CAFFE
	_LIB += VehicleDetection
	_DFLAGS += CAFFE_
	INCLUDE += -I $(CAFFE_DIR)/include
	LIBRARY += -L $(CAFFE_DIR)/lib -lcaffe -lboost_system -lglog -lgflags
endif

####### Files & directories

TARGETS = DriverOS Test

_SRC = Source $(addprefix Source/,$(_LIB))
SRC = $(wildcard $(addsuffix /*.cpp,$(_SRC)))
DEP = $(patsubst Source/%.cpp,Build/dep/%.d,$(SRC))
OBJ = $(patsubst Source/%.cpp,Build/obj/%.o,$(SRC))
LIB = $(patsubst %,Build/lib/lib%.a,$(_LIB))
DIRS = $(addprefix ./,Build $(addprefix Build/,bin lib dep $(addprefix dep/,$(_LIB)) obj $(addprefix obj/,$(_LIB))))
BIN = $(addprefix Build/bin/,$(TARGETS))

####### Compiler/linker options

CXX := g++

DFLAGS := $(addprefix -D ,$(_DFLAGS))
OFLAGS := -O3
WFLAGS := -Wall
CFLAGS := $(OFLAGS) $(WFLAGS) $(DFLAGS) -std=c++11 $(INCLUDE)
LFLAGS := $(OFLAGS) $(WFLAGS) $(LIBRARY)

####### Build rules

.DEFAULT_GOAL := all
.PHONY : clean debug profile coverage print

# TODO(36): Clean everything if Makefile.config changed
all : $(BIN)

DriverOS : Build/bin/DriverOS
Test : Build/bin/Test

obj : $(OBJ)

lib : $(LIB)

clean :
	@rm -rf Build/
	@rm -rf Distribute/

print :
	@echo $(LIBRARY) $(LIBDIR)

debug : OFLAGS := -Og -Wall
debug : all

profile : OFLAGS += -pg -fprofile-arcs
profile : all

coverage : OFLAGS += --coverage
coverage : all


####### Directory targets

Build/ :
	@echo $@
	@mkdir $(DIRS)
Distribute : all
	@echo $@
	@mkdir -p Distribute
	@cp -r Build/bin Distribute
	@cp -r Build/lib Distribute
	@mkdir -p Distribute/include
	@cp -r $(addprefix Source/,$(addsuffix /*.h,$(_LIB))) Distribute/include


####### Auto-dependencies

-include $(DEP)


####### Compile source files

Build/obj/%.o : Source/%.cpp | Build/
	@echo $@
	$(eval D = $(patsubst Build/obj/%.o,Build/dep/%.d,$@))
	$(eval V = $(shell $(CXX) $(CFLAGS) -MM $< | tr -d '\\'))
	@if [ -n "$(V)" ]; then { printf "$(@D)/"; printf "$(V)\n"; } > $(D); fi
	@$(CXX) -c $(CFLAGS) -o $@ $<


####### Link libraries

ps:=%
.SECONDEXPANSION :
Build/lib/lib%.a : $$(patsubst Source/$$(ps).cpp,Build/obj/$$(ps).o,$$(wildcard Source/%/*.cpp))
	@echo $@
	@ar rcs $@ $^


####### Link binaries

Build/bin/% : $(LIB) $(OBJ)
	@echo $@
	$(eval B = $(patsubst Build/bin/%,Build/obj/%.o,$(BIN)))
	$(eval T = $(patsubst Build/bin/%,Build/obj/%.o,$@))
	$(eval O = $(filter-out $(filter-out $(T),$(B)),$(OBJ)))
	@$(CXX) $(O) $(LIB) $(LFLAGS) -o $@

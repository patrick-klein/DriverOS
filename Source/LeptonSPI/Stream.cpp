/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Stream.cpp                                                                        **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "Stream.h"

#include "Lepton_SPI.h"
#include "Palettes.h"

using namespace std;
using namespace SPI1;

StreamObject::StreamObject()
{
    _last_update = 0;
    SPI_open_port();
}

StreamObject::~StreamObject()
{
    SPI_close_port();
}

void StreamObject::gray_image(Mat& image)
{
    image.create(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8U);
}

void StreamObject::color_image(Mat& image)
{
    image.create(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3);
}

int StreamObject::next(Mat image)
{
    // wait for next update
    // clock_t nextUpdate = _last_update + 1.0/FPS*CLOCKS_PER_SEC;
    // int waitTimeUS = 1e6 * (nextUpdate-clock()) / CLOCKS_PER_SEC;
    // if(waitTimeUS>0){
    //     usleep(waitTimeUS);
    // }

    // Read data packets from Lepton over SPI
    uint8_t result[PACKET_SIZE * PACKETS_PER_FRAME];
    uint8_t packet[PACKET_SIZE];
    int packetNumber;
    int resets = 0;
    for (int j = 0; j < PACKETS_PER_FRAME; j++)
    {
        read(spi_fd, result + sizeof(uint8_t) * PACKET_SIZE * j, sizeof(uint8_t) * PACKET_SIZE);
        packetNumber = result[j * PACKET_SIZE + 1];
        // If it's a drop packet, reset j to 0, set to -1 so it will start at 0 again
        if (packetNumber != j)
        {
            j = -1;
            resets += 1;
            usleep(1000);
            /*
             * Note: we've selected 750 resets as an arbitrary limit,
             * since there should never be 750 "null"
             * packets between two valid transmissions at the current poll rate
             * By polling faster, developers may easily exceed this count,
             * and the down period between frames may then be flagged
             * as a loss of sync
             */
            if (resets == 750)
            {
                cout << "Re-opening SPI port..." << endl;
                SPI_close_port();
                usleep(750000);
                SPI_open_port();
                resets = 0;
            }
        }
    }
    _last_update = clock();

    // Parse and format raw results
    uint16_t* frameBuffer = (uint16_t*) result;
    int row, column;
    uint16_t value;
    uint16_t minValue = 65535;
    uint16_t maxValue = 0;
    for (int i = 0; i < FRAME_SIZE_UINT16; i++)
    {
        // Skip the first 2 uint16_t's of every packet, they're 4 header bytes
        if (i % PACKET_SIZE_UINT16 < 2)
        {
            continue;
        }

        // Flip the MSB and LSB at the last second
        int temp = result[i * 2];
        result[i * 2] = result[i * 2 + 1];
        result[i * 2 + 1] = temp;

        value = frameBuffer[i];
        if (value > maxValue)
        {
            maxValue = value;
        }
        if (value < minValue)
        {
            minValue = value;
        }
        column = i % PACKET_SIZE_UINT16 - 2;
        row = i / PACKET_SIZE_UINT16;
    }

    static uint16_t avgMax = maxValue;
    static uint16_t avgMin = minValue;
    avgMax = 0.9 * avgMax + 0.1 * maxValue;
    avgMin = 0.9 * avgMin + 0.1 * minValue;

    // insert values into image matrix
    float fVal;
    float diff = avgMax - avgMin;
    float scale = 255 / diff;
    for (int i = 0; i < FRAME_SIZE_UINT16; i++)
    {
        if (i % PACKET_SIZE_UINT16 < 2)
        {
            continue;
        }
        fVal = (frameBuffer[i] - avgMin) * scale;
        if (fVal < 0)
        {
            fVal = 0;
        }
        else if (fVal > 255)
        {
            fVal = 255;
        }
        column = (i % PACKET_SIZE_UINT16) - 2;
        row = i / PACKET_SIZE_UINT16;
        image.at<uint8_t>(row, column) = fVal;
    }

    return 0;
}

int StreamObject::colorize(const Mat src, Mat dst)
{
    // Insert color values into image matrix
    const int* colormap = colormap_ironblack;
    for (int i = 0; i < src.rows; i++)
    {
        for (int j = 0; j < src.cols; j++)
        {
            int value = src.at<uint8_t>(i, j);
            dst.at<Vec3b>(i, j)[2] = colormap[3 * value];
            dst.at<Vec3b>(i, j)[1] = colormap[3 * value + 1];
            dst.at<Vec3b>(i, j)[0] = colormap[3 * value + 2];
        }
    }

    return 0;
}

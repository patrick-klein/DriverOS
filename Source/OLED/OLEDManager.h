/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Test.cpp                                                                          **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#ifndef OLED_OLEDMANAGER_H_
#define OLED_OLEDMANAGER_H_

#include "GPIOManager.h"

#define OLED_WIDTH 64
#define OLED_HEIGHT 48
#define OLED_SIZE OLED_WIDTH* OLED_HEIGHT / 8

#define SETCONTRAST             0x81
#define DISPLAYALLONRESUME      0xA4
#define DISPLAYALLON            0xA5
#define NORMALDISPLAY           0xA6
#define INVERTDISPLAY           0xA7
#define DISPLAYOFF              0xAE
#define DISPLAYON               0xAF
#define SETDISPLAYOFFSET        0xD3
#define SETCOMPINS              0xDA
#define SETVCOMDESELECT         0xDB
#define SETDISPLAYCLOCKDIV      0xD5
#define SETPRECHARGE            0xD9
#define SETMULTIPLEX            0xA8
#define SETLOWCOLUMN            0x00
#define SETHIGHCOLUMN           0x10
#define SETSTARTLINE            0x40
#define MEMORYMODE              0x20
#define COMSCANINC              0xC0
#define COMSCANDEC              0xC8
#define SEGREMAP                0xA0
#define CHARGEPUMP              0x8D
#define EXTERNALVCC             0x01
#define SWITCHCAPVCC            0x02

typedef unsigned char oled_image[OLED_WIDTH* OLED_HEIGHT];
typedef unsigned char oled_buf[OLED_SIZE];

class OLEDManager
{
 public:
    OLEDManager();
    ~OLEDManager();
    int display_image(oled_image);
    int clear_display();
    int test_image();
    int send_command(const unsigned char comm_buf[], const int num_bytes_to_send);

 private:
    GPIO::GPIOManager* gp;
    int rst;
    int dc;
    int send_data(const unsigned char data_buf[], const int num_bytes_to_send);
    int set_page_address(unsigned char addr);
    int set_column_address(unsigned char addr);
};

#endif  // OLED_OLEDMANAGER_H_

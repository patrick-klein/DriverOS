/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    DriverOS.cpp                                                                      **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>

#include "cxxopts.hpp"
#include "Display.h"
#include "LaneGuidance/test_lanes.h"
#ifdef LEPTON
    #include "LeptonI2C/Lepton_I2C.h"
    #include "LeptonSPI/Stream.h"
#endif
#ifdef OBD2
    #include "OBD2/OBD2Manager.h"
#endif
#ifdef OLED
    #include "OLED/OLEDManager.h"
#endif

using cv::Mat;
using std::cout;
using std::endl;
using std::string;
using std::vector;

int main(int argc, char** argv)
{
    // Parse command line arguments and set flags
    cxxopts::Options options("DriverOS", "Embedded Platform for Driver-Assist Features");
    options
        .allow_unrecognised_options()
        .add_options()
        ("h,help", "Print help");
    vector<string> groups = {""};
    #ifdef LEPTON
        groups.push_back("Lepton");
        options.add_options("Lepton")
            ("ffc", "Run FFC Normalization")
            ("agc", "Turn on AGC")
            ("agc-off", "Turn off AGC")
            ("save", "Save image")
            ("reboot", "Reboot device");
    #endif
    #ifdef OLED
        groups.push_back("OLED");
        options.add_options("OLED")
            ("wipe", "Wipe display");
    #endif
    #if defined(LEPTON) && defined(OLED)
        groups.push_back("Lepton + OLED");
        options.add_options("Lepton + OLED")
            ("lepton-oled", "Display Lepton stream on OLED");
    #endif
    #if defined(LEPTON) && defined(DISPLAY)
        groups.push_back("Lepton + Display");
        options.add_options("Lepton + Display")
            ("display", "Display stream from Lepton")
            ("capture", "Captures and displays stream from Lepton");
    #endif
    auto result = options.parse(argc, argv);

    // Show all available command line options
    if (result.count("help"))
    {
        cout << options.help(groups) << endl;
        exit(0);
    }

    #ifdef LEPTON
        // Lepton FFC
        if (result.count("ffc"))
        {
            cout << "Running FFC... " << flush;
            lepton_perform_ffc();
            cout << "FFC Done" << endl;
        }

        // Turn on AGC
        if (result.count("agc"))
        {
            int agc_state {lepton_get_agc_state()};
            if (!agc_state)
            {
                cout << "Turning on AGC... " << flush;
                lepton_set_agc_state(1);
                agc_state = lepton_get_agc_state();
                if (!agc_state)
                {
                    cout << "Failed to turn on AGC" << endl;
                }
                else
                {
                    cout << "AGC On" << endl;
                }
            }
            else if (agc_state)
            {
                cout << "AGC On" << endl;
            }
        }

        // Turn off AGC
        if (result.count("agc-off"))
        {
            int agc_state {lepton_get_agc_state()};
            if (agc_state)
            {
                cout << "Turning off AGC... " << flush;
                lepton_set_agc_state(0);
                agc_state = lepton_get_agc_state();
                if (agc_state)
                {
                    cout << "Failed to turn off AGC" << endl;
                }
                else
                {
                    cout << "AGC Off" << endl;
                }
            }
            else
            {
                cout << "AGC Off" << endl;
            }
        }

        // Save image from Lepton
        if (result.count("save"))
        {
            cout << "Saving image..." << endl;
            StreamObject stream;
            Mat gray;
            stream.gray_image(gray);
            stream.next(gray);
            imwrite("./Output/gray.png", gray);
            Mat color;
            stream.color_image(color);
            stream.colorize(gray, color);
            imwrite("./Output/color.png", color);
        }

        // Reboot device
        if (result.count("reboot"))
        {
            cout << "Rebooting Lepton..." << endl;
            lepton_do_reboot();
            cout << "Done" << endl;
        }
    #endif

    #ifdef OLED
        // Wipe OLED
        if (result.count("wipe"))
        {
            cout << "Wiping OLED... " << flush;
            OLEDManager oled;
            oled.clear_display();
            cout << "Done" << endl;
        }
    #endif

    #if defined(LEPTON) && defined(OLED)
        // Stream to OLED display
        if (result.count("lepton-oled"))
        {
            cout << "Streaming to OLED..." << endl;
            display_OLED();
        }
    #endif

    #if defined(LEPTON) && defined(DISPLAY)
        // Display Stream from Lepton and Save
        if (result.count("capture"))
        {
            cout << "Capturing stream..." << endl;
            capture_stream();
            cout << "Done" << endl;
        }

        // Display Stream from Lepton
        if (result.count("display"))
        {
            cout << "Streaming to display..." << endl;
            display_stream();
        }
    #endif

    return 0;
}

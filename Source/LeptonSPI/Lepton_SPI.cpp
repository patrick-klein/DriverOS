/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * MODIFIED: Patrick Klein, 2018
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 */

#include "Lepton_SPI.h"

namespace SPI1 {
int spi_fd = -1;

unsigned char spi_mode = SPI_MODE_3;
unsigned char spi_bits_per_word = 8;
unsigned int spi_speed = 10000000;

int SPI_open_port()
{
    int status_value = -1;
    int* spi_fd_ptr;

    // ----- SET SPI MODE -----
    // SPI_MODE_0 (0,0)
    //     CPOL=0 (Clock Idle low level)
    //     CPHA=0 (SDO transmit/change edge active to idle)
    // SPI_MODE_1 (0,1)
    //     CPOL=0 (Clock Idle low level)
    //     CPHA=1 (SDO transmit/change edge idle to active)
    // SPI_MODE_2 (1,0)
    //     CPOL=1 (Clock Idle high level)
    //     CPHA=0 (SDO transmit/change edge active to idle)
    // SPI_MODE_3 (1,1)
    //     CPOL=1 (Clock Idle high level)
    //     CPHA=1 (SDO transmit/change edge idle to active)
    spi_mode = SPI_MODE_3;

    // ----- SET BITS PER WORD -----
    spi_bits_per_word = 8;

    // ----- SET SPI BUS SPEED -----
    spi_speed = 10000000;  // 1000000 = 1MHz (1uS per bit)

    // Set pointer to spi_fd and open file
    spi_fd_ptr = &spi_fd;
    *spi_fd_ptr = open(LEPTON_SPI, O_RDWR);

    if (*spi_fd_ptr < 0)
    {
        perror("Error - Could not open SPI device");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_WR_MODE, &spi_mode);
    if (status_value < 0)
    {
        perror("Could not set SPIMode (WR)...ioctl fail");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_RD_MODE, &spi_mode);
    if (status_value < 0)
    {
        perror("Could not set SPIMode (RD)...ioctl fail");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_WR_BITS_PER_WORD, &spi_bits_per_word);
    if (status_value < 0)
    {
        perror("Could not set SPI bitsPerWord (WR)...ioctl fail");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_RD_BITS_PER_WORD, &spi_bits_per_word);
    if (status_value < 0)
    {
        perror("Could not set SPI bitsPerWord(RD)...ioctl fail");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_WR_MAX_SPEED_HZ, &spi_speed);
    if (status_value < 0)
    {
        perror("Could not set SPI speed (WR)...ioctl fail");
        exit(1);
    }

    status_value = ioctl(*spi_fd_ptr, SPI_IOC_RD_MAX_SPEED_HZ, &spi_speed);
    if (status_value < 0)
    {
        perror("Could not set SPI speed (RD)...ioctl fail");
        exit(1);
    }

    return status_value;
}

int SPI_close_port()
{
    int status_value = -1;
    int* spi_fd_ptr;

    spi_fd_ptr = &spi_fd;


    status_value = close(*spi_fd_ptr);
    if (status_value < 0)
    {
        perror("Error - Could not close SPI device");
        exit(1);
    }
    return status_value;
}
}  // namespace SPI1

/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    OBD2Manager.cpp                                                                   **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "OBD2Manager.h"

OBD2Manager::OBD2Manager()
{
    // -------------------------
    // ----- SETUP USART 0 -----
    // -------------------------
    // At bootup, pins 8 and 10 are already set to
    // UART0_TXD, UART0_RXD (ie the alt0 function) respectively
    uart_fd = -1;

    // OPEN THE UART
    // The flags (defined in fcntl.h):
    //    Access modes (use 1 of these):
    //        O_RDONLY - Open for reading only.
    //        O_RDWR - Open for reading and writing.
    //        O_WRONLY - Open for writing only.
    //
    //    O_NDELAY / O_NONBLOCK (same function) - Enables nonblocking mode. When set read requests
    //                                            on the file can return immediately with a failure
    //                                            status if there is no input immediately available
    //                                            (instead of blocking). Likewise, write requests
    //                                            can also return immediately with a failure status
    //                                            if the output can't be written immediately.
    //
    //    O_NOCTTY - When set and path identifies a terminal device, open() shall not cause the
    //               terminal device to become the controlling terminal for the process.
    // Open in non blocking read/write mode
    uart_fd = open(OBD2_UART, O_RDWR | O_NOCTTY | O_NDELAY);
    if (uart_fd == -1)
    {
        // ERROR - CAN'T OPEN SERIAL PORT
        printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
    }

    // CONFIGURE THE UART
    // The flags (defined in /usr/include/termios.h - see
    // http://pubs.opengroup.org/onlinepubs/007908799/xsh/termios.h.html):
    //    Baud rate - B1200, B2400, B4800, B9600, B19200, B38400, B57600, B115200, B230400, B460800,
    //                B500000, B576000, B921600, B1000000, B1152000, B1500000, B2000000, B2500000,
    //                B3000000, B3500000, B4000000
    //    CSIZE - CS5, CS6, CS7, CS8
    //    CLOCAL - Ignore modem status lines
    //    CREAD - Enable receiver
    //    IGNPAR - Ignore characters with parity errors
    //    ICRNL - Map CR to NL on input (Use for ASCII comms where you want to auto correct end of
    //            line characters - don't use for bianry comms!)
    //    PARENB - Parity enable
    //    PARODD - Odd parity (else even)
    struct termios options;
    tcgetattr(uart_fd, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;  // <Set baud rate
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart_fd, TCIFLUSH);
    tcsetattr(uart_fd, TCSANOW, &options);
}


OBD2Manager::~OBD2Manager()
{
    close(uart_fd);
}



int OBD2Manager::request(buffer_t tx_buf, buffer_t rx_buf)
{
    // ----- TX BYTES -----
    int count = write(uart_fd, tx_buf, strlen(tx_buf));
    if (count < 0)
    {
        printf("UART TX error\n");
    }

    usleep(5e6);

    // ----- CHECK FOR ANY RX BYTES -----
    // Read up to 255 characters from the port if they are there
    int rx_len = read(uart_fd, rx_buf, 255);
    if (rx_len < 0)
    {
        // An error occured (will occur if there are no bytes)
        printf("No bytes received\n");
    }
    else if (rx_len == 0)
    {
        // No data waiting
        printf("No data waiting\n");
    }
    else
    {
        // Bytes received
        rx_buf[rx_len] = '\0';
        printf("%i bytes read : %s\n", rx_len, rx_buf);
    }

    return 0;
}

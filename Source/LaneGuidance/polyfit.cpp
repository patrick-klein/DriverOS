/*
 * Copyright (c) unknown
 *
 * SOURCE: https://github.com/natedomin/polyfit
 *
 * MODIFIED: Patrick Klein, 2018
 *
 * ----------------------------------------------------
 *
 * METHOD:  polyfit
 *
 * TODO(...): Fix inputs description
 *
 * INPUTS:  dependentValues[0..(countOfElements-1)]
 *         independentValues[0...(countOfElements-1)]
 *         countOfElements
 *         order - Order of the polynomial fitting
 *
 * OUTPUTS: coeffs[0..order] - indexed by term
 *              (the (coef*x^3) is coeffs[3])
 *
 * ----------------------------------------------------
 */

#include "polyfit.h"

#include <array>
#include <vector>

#include <opencv2/core/core.hpp>

using cv::Point2f;
using std::array;
using std::vector;

int polyfit(const vector<Point2f>& points,
            array<float, 3>* coeffs)
{
    // Declarations...
    // ----------------------------------
    enum {max_order = 5};
    unsigned int order = 2;

    float B[max_order + 1] = {0.0f};
    float P[((max_order + 1) * 2) + 1] = {0.0f};
    float A[(max_order + 1) * 2 * (max_order + 1)] = {0.0f};

    float y, x, powy;

    unsigned int ii, jj, kk;

    // Verify initial conditions....
    // ----------------------------------

    // This method requires that the points.size() > (order+1)
    if (points.size() <= order)
        return -1;

    // This method has imposed an arbitrary bound of order <= max_order.
    // Increase max_order if necessary.
    if (order > max_order)
        return -1;

    // Begin Code...
    // ----------------------------------

    // Identify the column vector
    for (ii = 0; ii < points.size(); ii++)
    {
        y = points[ii].y;
        x = points[ii].x;
        powy = 1;

        for (jj = 0; jj < (order + 1); jj++)
        {
            B[jj] = B[jj] + (x * powy);
            powy = powy * y;
        }
    }

    // Initialize the PowX array
    P[0] = points.size();

    // Compute the sum of the Powers of X
    for (ii = 0; ii < points.size(); ii++)
    {
        y = points[ii].y;
        powy = points[ii].y;

        for (jj = 1; jj < ((2 * (order + 1)) + 1); jj++)
        {
            P[jj] = P[jj] + powy;
            powy = powy * y;
        }
    }

    // Initialize the reduction matrix
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            A[(ii * (2 * (order + 1))) + jj] = P[ii + jj];
        }
        A[(ii * (2 * (order + 1))) + (ii + (order + 1))] = 1;
    }

    // Move the identity matrix portion of the redux matrix to the left side
    // (Find the inverse of the left side of the redux matrix)
    for (ii = 0; ii < (order + 1); ii++)
    {
        y = A[(ii * (2 * (order + 1))) + ii];
        if (x != 0)
        {
            for (kk = 0; kk < (2 * (order + 1)); kk++)
            {
                A[(ii * (2 * (order + 1))) + kk] = A[(ii * (2 * (order + 1))) + kk] / y;
            }
            for (jj = 0; jj < (order + 1); jj++)
            {
                if ((jj - ii) != 0)
                {
                    x = A[(jj * (2 * (order + 1))) + ii];
                    for (kk = 0; kk < (2 * (order + 1)); kk++)
                    {
                        A[(jj * (2 * (order + 1))) + kk] = A[(jj * (2 * (order + 1))) + kk] - x *
                                                           A[(ii * (2 * (order + 1))) + kk];
                    }
                }
            }
        }
        else
        {
            // Cannot work with singular matrices
            return -1;
        }
    }

    // Calculate and identify the coeffs
    for (ii = 0; ii < (order + 1); ii++)
    {
        for (jj = 0; jj < (order + 1); jj++)
        {
            y = 0;
            for (kk = 0; kk < (order + 1); kk++)
            {
                y = y + (A[(ii * (2 * (order + 1))) + (kk + (order + 1))] * B[kk]);
            }
            (*coeffs)[ii] = y;
        }
    }

    return 0;
}

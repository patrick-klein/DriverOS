# DriverOS

#### Embedded Platform for Driver-Assist Features

DriverOS is an open source project that provides several libraries for driver-assist features.  Each library can be built independently, or linked together into the main _`DriverOS`_ program to provide a single interface for all available features.

Compilation on various hardware platforms is designed to be easy, with only a single config file to edit.  See the `Makefile.config` for a full list and description of available options. Preset config files are available for a desktop test environment, BeagleBone Black, and Nvidia Jetson TK1.

The follow table shows what libraries / features are available with the given options:

|                                                     | Caffe | Display | FLIR Lepton | Googletest | OBD-II Diagnostics | OLED Display | Video Input |
| --------------------------------------------------- | ----- | ------- | ----------- | ---------- | ------------------ | ------------ | ----------- |
| **Libraries**                                       |       |         |             |            |                    |              |             |
| LaneGuidance                                        |       |         |             |            |                    |              |             |
| LeptonI2C                                           |       |         | x           |            |                    |              |             |
| LeptonSPI                                           |       |         | x           |            |                    |              |             |
| OBD2                                                |       |         |             |            | x                  |              |             |
| OLED                                                |       |         |             |            |                    | x            |             |
| VehicleDetection                                    | x     |         |             |            |                    |              |             |
| **Features**                                        |       |         |             |            |                    |              |             |
| Do lane detection on video file and save output     |       |         |             |            |                    |              |             |
| Do lane detection on live video and save output     |       |         |             |            |                    |              | x           |
| Do lane detection on video file and display on OLED |       |         |             |            |                    | x            |             |
| Find vehicles in video file and save output         | x     |         |             |            |                    |              |             |
| Find objects in image file and save output          | x     |         |             |            |                    |              |             |
| Save live video input                               |       |         |             |            |                    |              | x           |
| Display IR stream                                   |       | x       | x           |            |                    |              |             |
| Display and save IR stream                          |       | x       | x           |            |                    |              |             |
| Display IR stream on OLED                           |       |         | x           |            |                    | x            |             |
| Combine video and IR inputs and save output         |       |         | x           |            |                    |              | x           |
| IR FFC Normalization                                |       |         | x           |            |                    |              |             |
| IR AGC Toggle                                       |       |         | x           |            |                    |              |             |
| Save IR Image                                       |       |         | x           |            |                    |              |             |
| Reboot FLIR Lepton                                  |       |         | x           |            |                    |              |             |
| Clear OLED Display                                  |       |         |             |            |                    | x            |             |
| Do a simple send/receive test for OBD-II library    |       |         |             |            | x                  |              |             |
| Run unit tests                                      |       |         |             | x          |                    |              |             |
|                                                     | Caffe | Display | FLIR Lepton | Googletest | OBD-II Diagnostics | OLED Display | Video Input |

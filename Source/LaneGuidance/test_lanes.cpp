/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    test_lanes.cpp                                                                     **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "test_lanes.h"

#include <cstdio>
#include <iostream>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "LaneFinder.h"

using cv::COLOR_BGR2GRAY;
using cv::FONT_HERSHEY_SIMPLEX;
using cv::Mat;
using cv::Scalar;
using cv::Size;
using cv::Point;
using cv::Point2f;
using cv::VideoCapture;
using cv::VideoWriter;
using std::cout;
using std::endl;
using std::flush;
using std::string;


void test_read_video_file()
{
    VideoCapture cap("./test_video.mp4");
    if (!cap.isOpened())
    {
        return;
    }

    char charBuf[100];

    Mat edges;
    for (int i {0}; i < 10; i++)
    {
        Mat frame;
        cap >> frame;
        cvtColor(frame, edges, COLOR_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5);
        Canny(edges, edges, 0, 30, 3);
        snprintf(charBuf, sizeof(charBuf), "./Output/lanes_test_%u.jpg", i);
        imwrite(charBuf, edges);
    }
}

void test_LaneFinder(string filename)
{
    VideoCapture cap(filename);
    if (!cap.isOpened())
    {
        cout << "Unable to open video file!";
        return;
    }

    int rows {static_cast<int>(cap.get(CV_CAP_PROP_FRAME_HEIGHT))};
    int cols {static_cast<int>(cap.get(CV_CAP_PROP_FRAME_WIDTH))};
    Mat frame {rows, cols, CV_8UC3};
    Mat left_lane_line, right_lane_line, lane_area, mask, unwarped1, unwarped2, unwarped3, final;
    Mat channels[3];
    char charBuf[255];
    LaneFinder lanes {rows, cols};

    for (int i = 0; i < 10; i++)
    {
        cap.read(frame);
        lanes.find_lanes(frame);
        lanes.get_lane_lines(LEFT_LANE, &left_lane_line);
        lanes.get_lane_lines(RIGHT_LANE, &right_lane_line);
        lanes.get_lane_area(&lane_area);

        channels[0] = Mat::zeros(rows, cols, CV_8U);
        channels[1] = 255 * lane_area;
        channels[2] = Mat::zeros(rows, cols, CV_8U);
        merge(channels, 3, mask);

        lanes.unwarp_image(mask, &unwarped1);
        lanes.unwarp_image(left_lane_line, &unwarped2);
        lanes.unwarp_image(right_lane_line, &unwarped3);

        addWeighted(frame, 1, unwarped1, 0.3, 0, final);
        final.setTo(Scalar(0, 0, 255), unwarped3 > 0);
        final.setTo(Scalar(255, 0, 0), unwarped2 > 0);

        snprintf(charBuf, sizeof(charBuf), "./Output/lanes_%ug_final.jpg", i);
        imwrite(charBuf, final);
    }
}

void test_LaneFinder_video(string filename)
{
    VideoCapture cap {filename};
    if (!cap.isOpened())
    {
        cout << "Unable to open video file to read!" << endl;
        return;
    }
    int rows {static_cast<int>(cap.get(CV_CAP_PROP_FRAME_HEIGHT))};
    int cols {static_cast<int>(cap.get(CV_CAP_PROP_FRAME_WIDTH))};
    int numFrames {static_cast<int>(cap.get(CV_CAP_PROP_FRAME_COUNT))};

    string outfile {"./Output/lanes_video.avi"};
    int fourcc {CV_FOURCC('M', 'J', 'P', 'G')};
    float fps {static_cast<float>(cap.get(CV_CAP_PROP_FPS))};
    Size frameSize(Size(cols, rows));
    VideoWriter video {outfile, fourcc, fps, frameSize};
    if (!video.isOpened())
    {
        cout << "Unable to open video file to write!" << endl;
        return;
    }

    Mat frame {rows, cols, CV_8UC3};
    Mat left_lane_line, right_lane_line, lane_area, mask, unwarped1, unwarped2, unwarped3, final;
    Mat channels[3];
    LaneFinder lanes {rows, cols};
    float position, radius;

    char charBuf1[256];
    char charBuf2[10];
    int i {0};

    while (cap.read(frame))
    {
        lanes.find_lanes(frame);
        lanes.get_lane_lines(LEFT_LANE, &left_lane_line);
        lanes.get_lane_lines(RIGHT_LANE, &right_lane_line);
        lanes.get_lane_area(&lane_area);
        position = lanes.get_position();
        radius = lanes.get_radius();

        channels[0] = Mat::zeros(rows, cols, CV_8U);
        channels[1] = 255 * lane_area;
        channels[2] = Mat::zeros(rows, cols, CV_8U);
        merge(channels, 3, mask);

        lanes.unwarp_image(mask, &unwarped1);
        lanes.unwarp_image(left_lane_line, &unwarped2);
        lanes.unwarp_image(right_lane_line, &unwarped3);

        // final = Mat::zeros(rows, cols, CV_8UC3);
        // final.setTo(Scalar(0, 255, 0), unwarped1>0);
        addWeighted(frame, 0.3, unwarped1, 0.3, 0, final);
        final.setTo(Scalar(255, 0, 0), unwarped2 > 0);
        final.setTo(Scalar(0, 0, 255), unwarped3 > 0);

        // cvtColor(bin,final,CV_GRAY2BGR);

        snprintf(charBuf1, sizeof(charBuf1), "Radius of Curvature = %.1f(km)", radius / 1000);
        putText(final, charBuf1, Point(10, 25), FONT_HERSHEY_SIMPLEX,
                0.75, Scalar(255, 255, 255), 1, CV_AA);

        if (position < 0)
        {
            snprintf(charBuf2, sizeof(charBuf2), "left");
        }
        else
        {
            snprintf(charBuf2, sizeof(charBuf2), "right");
        }
        snprintf(charBuf1, sizeof(charBuf1),
                 "Position = %.1f(m) %s of center", abs(position), charBuf2);
        putText(final, charBuf1, Point(10, 50), FONT_HERSHEY_SIMPLEX,
                0.75, Scalar(255, 255, 255), 1, CV_AA);

        video.write(final);

        cout << "\t" << 100 * ++i / numFrames << "%            \r" << flush;
    }
    cout << endl;
}

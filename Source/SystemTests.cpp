/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    SystemTests.cpp                                                                   **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "SystemTests.h"

#include <unistd.h>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "Display.h"
#include "LaneGuidance/LaneFinder.h"  // NOLINT(build/include_alpha) -- false positive
#ifdef LEPTON
    #include "LeptonI2C/Lepton_I2C.h"
    #include "LeptonSPI/Stream.h"
#endif
#ifdef OLED
    #include "OLED/OLEDManager.h"
#endif
#ifdef CAFFE_
    #include "VehicleDetection/yolo.h"
#endif

using cv::COLOR_BGR2RGB;
using cv::FONT_HERSHEY_SIMPLEX;
using cv::imread;
using cv::Mat;
using cv::Point;
using cv::Rect;
using cv::Scalar;
using cv::Size;
using cv::VideoCapture;
using cv::VideoWriter;
using std::cout;
using std::endl;
using std::string;
using std::vector;

#ifdef VIDEO
    void test_live_video()
    {
        VideoCapture cap {VIDEO_ID};
        if (!cap.isOpened())
        {
            cout << "Unable to open video device: " << VIDEO_ID << endl;
            return;
        }

        cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
        // cap.set(CV_CAP_PROP_FPS, 30);

        cout << "Reading initial frame..." << endl;
        Mat frame;
        cap.read(frame);

        int rows {frame.rows};
        int cols {frame.cols};

        cout << "Initializing output..." << endl;
        string outfile {"./Output/test_live_video.avi"};
        int fourcc {CV_FOURCC('M', 'J', 'P', 'G')};
        double fps {30};
        // double fps = cap.get(CV_CAP_PROP_FPS);
        Size frameSize {cols, rows};
        VideoWriter video {outfile, fourcc, fps, frameSize};
        if (!video.isOpened())
        {
            cout << "Unable to open video file to write!" << endl;
            return;
        }

        for (int i {0}; i < fps * 10; i++)
        {
            cap.read(frame);
            video.write(frame);
            cout << i << endl;
        }
    }

    void test_live_lane_finder()
    {
        VideoCapture cap {VIDEO_ID};
        if (!cap.isOpened())
        {
            cout << "Unable to open video device: " << VIDEO_ID << endl;
            return;
        }

        cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
        cap.set(CV_CAP_PROP_FPS, 30);

        cout << "Reading initial frame..." << endl;
        Mat frame;
        cap.read(frame);

        int rows {frame.rows};
        int cols {frame.cols};

        cout << "Initializing output..." << endl;
        string outfile {"./Output/test_live_lanes.avi"};
        int fourcc {CV_FOURCC('M', 'J', 'P', 'G')};
        double fps {30};
        Size frameSize(cols, rows);
        VideoWriter video(outfile, fourcc, fps, frameSize);
        if (!video.isOpened())
        {
            cout << "Unable to open video file to write!" << endl;
            return;
        }

        Mat left_lane_line, right_lane_line, lane_area;
        Mat mask, unwarped1, unwarped2, unwarped3, final;
        Mat channels[3];

        char charBuf[255];
        LaneFinder lanes {rows, cols};
        float position, radius;

        for (int i {0}; i < fps * 10; i++)
        {
            cap.read(frame);

            lanes.find_lanes(frame);
            lanes.get_lane_lines(LEFT_LANE, &left_lane_line);
            lanes.get_lane_lines(RIGHT_LANE, &right_lane_line);
            lanes.get_lane_area(&lane_area);

            channels[0] = Mat::zeros(rows, cols, CV_8U);
            channels[1] = 255 * lane_area;
            channels[2] = Mat::zeros(rows, cols, CV_8U);
            merge(channels, 3, mask);

            lanes.unwarp_image(mask, &unwarped1);
            lanes.unwarp_image(left_lane_line, &unwarped2);
            lanes.unwarp_image(right_lane_line, &unwarped3);

            addWeighted(frame, 1, unwarped1, 0.3, 0, final);
            final.setTo(Scalar(0, 0, 255), unwarped3 > 0);
            final.setTo(Scalar(255, 0, 0), unwarped2 > 0);

            radius = lanes.get_radius();
            snprintf(charBuf, sizeof(charBuf), "Radius: %.1f(km)", radius / 1000);
            putText(final, charBuf, Point(10, 25), FONT_HERSHEY_SIMPLEX,
                    0.75, Scalar(255, 255, 255), 1, CV_AA);

            position = lanes.get_position();
            snprintf(charBuf, sizeof(charBuf), "Position: %+.2f(m)", position);
            putText(final, charBuf, Point(10, 50), FONT_HERSHEY_SIMPLEX,
                    0.75, Scalar(255, 255, 255), 1, CV_AA);

            video.write(final);

            cout << i << endl;
        }
    }
#endif

#ifdef CAFFE_
    void test_YOLO_image(string filename)
    {
        YOLODetector yolo;

        Mat img {imread(filename, -1)};
        vector<float> probs;
        vector<vector<float>> boxes;
        vector<string> classes;

        yolo.detect(&img, &probs, &boxes, &classes);

        img.convertTo(img, CV_8UC3, 255);
        cvtColor(img, img, COLOR_BGR2RGB);
        for (int i {0}; i < probs.size(); i++)
        {
            vector<float> box = boxes[i];
            cout << classes[i] << "\t" << probs[i] << "\t"
                 << box[0] << "\t" << box[1] << "\t" << box[2] << "\t" << box[3] << endl;
            rectangle(img, Rect(box[0] - box[2] / 2, box[1] - box[3] / 2, box[2], box[3]),
                      Scalar {255, 255, 255});
        }
        imwrite("./Output/yolo.jpg", img);
    }

    void test_YOLO_video(string filename)
    {
        VideoCapture cap {filename};
        if (!cap.isOpened())
        {
            cout << "Unable to open video file: " << filename << endl;
            return;
        }

        int rows {448};
        int cols {448};

        string outfile {"./Output/test_yolo_video.avi"};
        int fourcc {CV_FOURCC('M', 'J', 'P', 'G')};
        double fps {cap.get(CV_CAP_PROP_FPS)};
        Size frameSize {cols, rows};
        VideoWriter video {outfile, fourcc, fps, frameSize};
        if (!video.isOpened())
        {
            cout << "Unable to open video file to write!" << endl;
            return;
        }

        Mat frame;
        vector<float> probs;
        vector<vector<float>> boxes;
        vector<string> classes;
        YOLODetector yolo;
        yolo.threshold = 0.1;

        int i {0};
        while (cap.read(frame))
        {
            yolo.detect(&frame, &probs, &boxes, &classes);
            frame.convertTo(frame, CV_8UC3, 255);
            cvtColor(frame, frame, COLOR_BGR2RGB);
            // for (auto const& e: boxes)
            // rectangle(frame, Rect(e[0]-e[2]/2,e[1]-e[3]/2,e[2],e[3]), Scalar(255,255,255));
            for (int i {0}; i < boxes.size(); i++)
            {
                string c {classes[i]};
                if (c == "bicycle" || c == "bus" || c == "car" || c == "motorbike" ||
                    c == "person" || c == "train")
                {
                    vector<float> box = boxes[i];
                    rectangle(frame, Rect(box[0] - box[2] / 2, box[1] - box[3] / 2,
                                          box[2], box[3]), Scalar {255, 255, 255});
                }
            }

            video.write(frame);
            cout << i++ << endl;
        }
    }
#endif

#ifdef OLED
    void test_lanes_on_OLED(string filename)
    {
        VideoCapture cap {filename};
        if (!cap.isOpened())
            return;

        int rows {static_cast<float>(cap.get(CV_CAP_PROP_FRAME_HEIGHT))};
        int cols {static_cast<float>(cap.get(CV_CAP_PROP_FRAME_WIDTH))};
        Mat frame {rows, cols, CV_8UC3};
        Mat unwarped {rows, cols, CV_8U};
        Mat small, left_lane_line, right_lane_line;

        LaneFinder lanes {rows, cols};
        OLEDManager oled;

        while (cap.read(frame))
        {
            lanes.find_lanes(frame);
            lanes.get_lane_lines(LEFT_LANE, &left_lane_line);
            lanes.get_lane_lines(RIGHT_LANE, &right_lane_line);

            // resize(left_lane_line+right_lane_line, small, Size(OLED_WIDTH, OLED_HEIGHT));
            lanes.unwarp_image(left_lane_line + right_lane_line, &unwarped);
            resize(unwarped, small, Size(OLED_WIDTH, OLED_HEIGHT));
            oled.display_image(small.data);
        }
    }
#endif

#if defined(VIDEO) && defined(LEPTON)
    void test_video_and_lepton()
    {
        // Initialize Lepton stream
        // Use a frame buffer to match video camera
        StreamObject stream;
        Mat leptonFrameBuffer[5];
        for (int i {0}; i < 5; i++)
        {
            stream.gray_image(leptonFrameBuffer[i]);
        }

        // Initialize video stream
        VideoCapture cap {VIDEO_ID};
        if (!cap.isOpened())
        {
            cout << "Unable to open video device: " << VIDEO_ID << endl;
            return;
        }
        cap.set(CV_CAP_PROP_FRAME_WIDTH, 320);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
        cout << "Reading initial video frame..." << endl;
        Mat videoFrame;
        cap.read(videoFrame);

        // Initialize video output
        cout << "Initializing output..." << endl;
        string outfile {"./Output/test_video_lepton.avi"};
        int fourcc {CV_FOURCC('M', 'J', 'P', 'G')};
        VideoWriter video {outfile, fourcc, FPS, videoFrame.size()};
        if (!video.isOpened())
        {
            cout << "Unable to open video file to write!" << endl;
            return;
        }

        Mat temp_32FC1;
        Mat temp_32FC3;
        Mat temp_8UC1;
        Mat channels[3];

        for (int i {0}; i < 100 + 5; i++)
        {
            cap.read(videoFrame);
            stream.next(leptonFrameBuffer[i % 5]);
            if (i >= 5)
            {
                resize(leptonFrameBuffer[(i + 1) % 5], temp_32FC1, videoFrame.size());
                temp_32FC1.convertTo(temp_32FC1, CV_32FC1, 1. / 255);
                videoFrame.convertTo(temp_32FC3, CV_32FC3, 1. / 255);
                cvtColor(temp_32FC3, temp_32FC3, CV_BGR2YCrCb);
                split(temp_32FC3, channels);
                channels[0] = channels[0].mul(temp_32FC1);
                // channels[0] = temp_32FC1;
                merge(channels, 3, temp_32FC3);
                cvtColor(temp_32FC3, temp_32FC3, CV_YCrCb2BGR);
                temp_32FC3.convertTo(temp_8UC1, CV_8UC1, 255);
                video.write(temp_8UC1);
                cout << i - 5 << endl;
            }
        }
    }
#endif

/*
 * Copyright (c) 2014, Pure Engineering LLC
 *
 * SOURCE:
 * https://github.com/groupgets/LeptonModule/blob/master/software/beagleboneblack_video/Palettes.cpp
 *
 * MODIFIED: Patrick Klein, 2018
 */

#ifndef LEPTONSPI_PALETTES_H_
#define LEPTONSPI_PALETTES_H_

extern const int colormap_rainbow[];
extern const int colormap_grayscale[];
extern const int colormap_ironblack[];

#endif  // LEPTONSPI_PALETTES_H_

/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    yolo.h                                                                            **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#ifndef VEHICLEDETECTION_YOLO_H_
#define VEHICLEDETECTION_YOLO_H_

#include <string>
#include <vector>

#include <opencv2/core/core.hpp>

// Ignore warning in caffe
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedef"
#include <caffe/caffe.hpp>
#pragma GCC diagnostic pop

#define WIDTH 448
#define HEIGHT 448

#define NUM_CLASSES 20
#define N_COORD_BOX 4
#define GRID_SIZE 7
#define OUTPUT_SIZE 1470

#define CON_OFFSET GRID_SIZE* GRID_SIZE* NUM_CLASSES
#define NUM_BOXES (OUTPUT_SIZE - CON_OFFSET) / (GRID_SIZE * GRID_SIZE * (N_COORD_BOX + 1))
#define BOX_OFFSET CON_OFFSET + GRID_SIZE * GRID_SIZE * NUM_BOXES

class YOLODetector
{
 public:
    float threshold {0.2f};
    float iou_threshold {0.4f};
    const std::string class_arr[NUM_CLASSES] {"aeroplane", "bicycle", "bird", "boat", "bottle",
                                              "bus", "car", "cat", "chair", "cow", "diningtable",
                                              "dog", "horse", "motorbike", "person",
                                              "pottedplant", "sheep", "sofa", "train", "tvmonitor"
    };

    // TODO(30): Use a reduced model that only recognizes the following classes
    // const std::string class_arr[6] = {"bicycle", "bus", "car", "motorbike", "person", "train"};
    // VOC: 1 5 6 13 14 18

    YOLODetector();
    void detect(cv::Mat* img,
                std::vector<float>* probs,
                std::vector<std::vector<float>>* boxes,
                std::vector<std::string>* classes);

 private:
    void _preprocess(cv::Mat* img);
    std::vector<float> _forwardPass(cv::Mat* img);
    void _parse_output(std::vector<float>* output,
                       std::vector<float>* probs,
                       std::vector<std::vector<float>>* boxes,
                       std::vector<std::string>* classes);
    void _non_maxima_suppression(std::vector<float>* probs,
                                 std::vector<std::vector<float>>* boxes,
                                 std::vector<int>* class_nums);
    float _iou(const std::vector<float>& box1,
               const std::vector<float>& box2);

    inline int CLS_INDEX(int i, int j, int k)
    {
        return NUM_CLASSES * GRID_SIZE * i + NUM_CLASSES * j + k;
    }

    inline int CON_INDEX(int i, int j, int k)
    {
        return NUM_BOXES * GRID_SIZE * i + NUM_BOXES * j + k;
    }

    inline int BOX_INDEX(int i, int j, int k, int l)
    {
        return N_COORD_BOX * NUM_BOXES * GRID_SIZE * i + N_COORD_BOX * NUM_BOXES * j + N_COORD_BOX *
               k + l;
    }

    caffe::shared_ptr<caffe::Net<float>> _net;
};

#endif  // VEHICLEDETECTION_YOLO_H_

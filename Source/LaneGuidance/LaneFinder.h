/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    LaneFinder.h                                                                      **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#ifndef LANEGUIDANCE_LANEFINDER_H_
#define LANEGUIDANCE_LANEFINDER_H_

#include <array>
#include <memory>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "StackBuffer.h"

typedef enum
{
    LEFT_LANE = 0,
    RIGHT_LANE
} lane_side_t;

typedef enum
{
    METERS = 0,
    PIXELS
} units_t;

class LaneFinder
{
 public:
    LaneFinder(int rows, int cols);

    void find_lanes(const cv::Mat& frame);
    void get_lane_area(cv::Mat* lane_area);
    void get_lane_lines(const lane_side_t side, cv::Mat* lane_line);
    float get_position();
    float get_radius();
    void unwarp_image(const cv::Mat& src, cv::Mat* dst) const;

 private:
    int _rows;
    int _cols;
    float _xm_per_pix;
    float _ym_per_pix;
    cv::Mat _M;
    cv::Mat _M_inv;
    int _left_base;
    int _right_base;
    bool _coeffs_valid {false};

    std::array<float, 3> _left_coeffs_pixels;
    std::array<float, 3> _right_coeffs_pixels;
    std::array<float, 3> _left_coeffs_meters;
    std::array<float, 3> _right_coeffs_meters;

    const int _buff_size {2048};
    std::shared_ptr<StackBuffer<cv::Point2f>> p_right_buffer
    {new StackBuffer<cv::Point2f> {_buff_size}};
    std::shared_ptr<StackBuffer<cv::Point2f>> p_left_buffer
    {new StackBuffer<cv::Point2f> {_buff_size}};

    void _do_polytfit(const lane_side_t side,
                      const units_t units,
                      std::vector<cv::Point2f>* lane_points_ptr);
    void _find_base_with_histogram(const cv::Mat& bin);
    void _find_nonzero(const cv::Mat& bin, std::vector<cv::Point>* nonzero);
    void _sliding_windows(const std::vector<cv::Point> nonzero,
                          std::vector<cv::Point2f>* left_lane_points_ptr,
                          std::vector<cv::Point2f>* right_lane_points_ptr);
    void _threshold_image(const cv::Mat& src, cv::Mat* dst) const;
    void _warp_image(const cv::Mat& src, cv::Mat* dst) const;
};

#endif  // LANEGUIDANCE_LANEFINDER_H_

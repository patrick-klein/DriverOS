/*******************************************************************************
**
**    Copyright 2011,2012,2013 FLIR Systems - Commercial Vision Systems
**    All rights reserved.
**
**    MODIFIED: Patrick Klein
**
**    File NAME: bbb_I2C.h
**
**      AUTHOR:  Hart Thomson
**
**      CREATED: 9/25/2012
**
**      DESCRIPTION:
**
**      HISTORY:  9/25/2012 HT - Initial Draft
**
**      Proprietary - Company Only.
**
**      This document is controlled to FLIR Technology Level 2.
**      The information contained in this document pertains to a dual use product
**      Controlled for export by the Export Administration Regulations (EAR).
**      FLIR trade secrets contained herein are subject to disclosure restrictions
**      as a matter of law. Diversion contrary to US law is prohibited.
**      US Department of Commerce authorization is required prior to export or
**      transfer to foreign persons or parties and for uses otherwise prohibited.
**
*******************************************************************************/
#ifndef LEPTONI2C_DEVICE_I2C_H_
#define LEPTONI2C_DEVICE_I2C_H_


/******************************************************************************/
/** INCLUDE FILES                                                            **/
/******************************************************************************/

#include <errno.h>
#include <fcntl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <unistd.h>

/******************************************************************************/
/** EXPORTED DEFINES                                                         **/
/******************************************************************************/

/******************************************************************************/
/** EXPORTED TYPE DEFINITIONS                                                **/
/******************************************************************************/

/******************************************************************************/
/** EXPORTED PUBLIC DATA                                                     **/
/******************************************************************************/

/******************************************************************************/
/** EXPORTED PUBLIC FUNCTIONS                                                **/
/******************************************************************************/

extern LEP_RESULT DEV_I2C_MasterInit(LEP_UINT16 portID,
                                     LEP_UINT16* BaudRate);

extern LEP_RESULT DEV_I2C_MasterClose();

extern LEP_RESULT DEV_I2C_MasterReset(void);

extern LEP_RESULT DEV_I2C_MasterReadData(LEP_UINT16 portID,
                                         LEP_UINT8 deviceAddress,
                                         LEP_UINT16 regAddress,  // Lepton Register Address
                                         LEP_UINT16* readDataPtr,
                                         LEP_UINT16 wordsToRead,  // Number of 16-bit words to read
                                         LEP_UINT16* numWordsRead,  // Number of 16-bit words read
                                         LEP_UINT16* status);

extern LEP_RESULT DEV_I2C_MasterWriteData(LEP_UINT16 portID,
                                          LEP_UINT8 deviceAddress,
                                          LEP_UINT16 regAddress,  // Lepton Register Address
                                          LEP_UINT16* writeDataPtr,
                                          LEP_UINT16 wordsToWrite,  // Number of 16-bit words to
                                                                    // write
                                          LEP_UINT16* numWordsWritten,  // Number of 16-bit words
                                                                        // written
                                          LEP_UINT16* status);

extern LEP_RESULT DEV_I2C_MasterReadRegister(LEP_UINT16 portID,
                                             LEP_UINT8 deviceAddress,
                                             LEP_UINT16 regAddress,
                                             LEP_UINT16* regValue,  // Number of 16-bit words
                                                                    // written
                                             LEP_UINT16* status);

extern LEP_RESULT DEV_I2C_MasterWriteRegister(LEP_UINT16 portID,
                                              LEP_UINT8 deviceAddress,
                                              LEP_UINT16 regAddress,
                                              LEP_UINT16 regValue,  // Number of 16-bit words
                                                                    // written
                                              LEP_UINT16* status);

extern LEP_RESULT DEV_I2C_MasterStatus(void);

/******************************************************************************/

#endif  // LEPTONI2C_DEVICE_I2C_H_

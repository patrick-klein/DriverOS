/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Test.cpp                                                                          **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/


#include <unistd.h>
#include <cstdio>
#include <iostream>
#include <string>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "cxxopts.hpp"
#include "Display.h"
#include "LaneGuidance/test_lanes.h"
#include "SystemTests.h"
#ifdef GTEST_
    #include "run_gtest.h"
#endif
#ifdef LEPTON
    #include "LeptonI2C/Lepton_I2C.h"
    #include "LeptonSPI/Stream.h"
#endif
#ifdef OBD2
    #include "OBD2/OBD2Manager.h"
#endif
#ifdef OLED
    #include "OLED/OLEDManager.h"
#endif
#ifdef CAFFE_
    #include "VehicleDetection/yolo.h"
#endif

using std::cout;
using std::endl;
using std::string;
using std::vector;
// using namespace cv;

int main(int argc, char** argv)
{
    // Parse command line arguments and set flags
    cxxopts::Options options("Testing", "Manual Tests for DriverOS");
    options
        .allow_unrecognised_options()
        .add_options()
        ("help", "Print help")
        ("lanes-video", "Test lane detection in video",
        cxxopts::value<string>()->implicit_value("./Video/test_240.mp4"));
    vector<string> groups = {""};
    #ifdef VIDEO
        groups.push_back("Video");
        options.add_options("Video")
            ("live-video", "Test video input")
            ("live-lanes", "Test lane detection on live video input");
    #endif
    #ifdef GTEST_
        groups.push_back("gtest");
        options.add_options("gtest")
            ("gtest", "Do a simple gtest");
    #endif
    #ifdef LEPTON
        groups.push_back("Lepton");
        options.add_options("Lepton")
            ("ffc", "Run FFC Normalization")
            ("agc", "Turns on AGC")
            ("agc-off", "Turns off AGC")
            ("save", "Saves image")
            ("reboot", "Reboots device");
    #endif
    #ifdef OLED
        groups.push_back("OLED");
        options.add_options("OLED")
            ("lanes-oled", "Test lane detection in video on OLED",
            cxxopts::value<string>()->implicit_value("./Video/test_240.mp4"));
    #endif
    #ifdef OBD2
        groups.push_back("OBD-2");
        options.add_options("OBD-2")
            ("obd2", "Test OBD-2 communications");
    #endif
    #ifdef CAFFE_
        groups.push_back("Caffe");
        options.add_options("Caffe")
            ("yolo-image", "Test YOLO Detector on image",
            cxxopts::value<string>()->implicit_value("./Model/person.jpg"))
            ("yolo-video", "Test YOLO Detector on video",
            cxxopts::value<string>()->implicit_value("./Video/vehicle_480.mp4"));
    #endif
    #if defined(VIDEO) && defined(LEPTON)
        groups.push_back("Video + Lepton");
        options.add_options("Video + Lepton")
            ("video-lepton", "Test video and Lepton together");
    #endif
    #if defined(LEPTON) && defined(DISPLAY)
        groups.push_back("Lepton + Display");
        options.add_options("Lepton + Display")
            ("capture", "Captures and displays stream from Lepton");
    #endif
    auto result = options.parse(argc, argv);

    // Show all available command line options
    if (result.count("help"))
    {
        cout << options.help(groups) << endl;
        exit(0);
    }

    // Run test for lane guidance
    if (result.count("lanes-video"))
    {
        cout << "Testing LaneFinder..." << endl;
        test_LaneFinder_video(result["lanes-video"].as<string>());
        cout << "Done" << endl;
    }

    #ifdef LEPTON
        // Run Lepton FFC
        if (result.count("ffc"))
        {
            cout << "Running FFC... " << endl;
            lepton_perform_ffc();
            cout << "Done" << endl;
        }

        // Turn on AGC
        if (result.count("agc"))
        {
            agc_state {lepton_get_agc_state()};
            if (!agc_state)
            {
                cout << "Turning on AGC... " << flush;
                lepton_set_agc_state(1);
                agc_state = lepton_get_agc_state();
                if (!agc_state)
                {
                    cout << "Failed to turn on AGC" << endl;
                }
                else
                {
                    cout << "AGC On" << endl;
                }
            }
            else if (agc_state)
            {
                cout << "AGC On" << endl;
            }
        }

        // Turn off AGC
        if (result.count("agc-off"))
        {
            int agc_state {lepton_get_agc_state()};
            if (agc_state)
            {
                cout << "Turning off AGC... " << flush;
                lepton_set_agc_state(0);
                agc_state = lepton_get_agc_state();
                if (agc_state)
                {
                    cout << "Failed to turn off AGC" << endl;
                }
                else
                {
                    cout << "AGC Off" << endl;
                }
            }
            else
            {
                cout << "AGC Off" << endl;
            }
        }

        // Save image from Lepton
        if (result.count("save"))
        {
            cout << "Saving image..." << endl;
            StreamObject stream;
            Mat gray;
            stream.gray_image(gray);
            stream.next(gray);
            imwrite("./Output/gray.png", gray);
            Mat color;
            stream.color_image(color);
            stream.colorize(gray, color);
            imwrite("./Output/color.png", color);
        }

        // Reboot device
        if (result.count("reboot"))
        {
            cout << "Rebooting Lepton..." << endl;
            lepton_do_reboot();
            cout << "Done" << endl;
        }
    #endif

    #ifdef OLED
        // Lanes test on OLED
        if (result.count("lanes-oled"))
        {
            cout << "Testing lane guidance on OLED... " << flush;
            test_lanes_on_OLED(result["lanes-oled"].as<string>());
            cout << "Done" << endl;
        }
    #endif

    #ifdef OBD2
        // Test OBD-II communications
        if (result.count("obd2"))
        {
            cout << "Testing OBD-2..." << endl;
            OBD2Manager obd2;
            buffer_t tx_buf {"HI"};
            buffer_t rx_buf;
            obd2.request(tx_buf, rx_buf);
            cout << "Done" << endl;
        }
    #endif

    #ifdef CAFFE_
        // Test YOLO on image*/
        if (result.count("yolo-image"))
        {
            cout << "Testing YOLO detector on image..." << endl;
            test_YOLO_image(result["yolo-image"].as<string>());
            cout << "Done" << endl;
        }


        // Test YOLO on video*/
        if (result.count("yolo-video"))
        {
            cout << "Testing YOLO detector on video..." << endl;
            test_YOLO_video(result["yolo-video"].as<string>());
            cout << "Done" << endl;
        }
    #endif

    #ifdef VIDEO
        // Test video input
        if (result.count("live-video"))
        {
            cout << "Testing live video..." << endl;
            test_live_video();
            cout << "Done" << endl;
        }

        // Test lane guidance on live video
        if (result.count("live-lanes"))
        {
            cout << "Testing live lane guidance..." << endl;
            test_live_lane_finder();
            cout << "Done" << endl;
        }
    #endif

    #ifdef GTEST_
        // Test simple gtest
        if (result.count("gtest"))
        {
            cout << "Testing gtest..." << endl;
            run_gtest(&argc, argv);
            cout << "Done" << endl;
        }
    #endif

    #if defined(VIDEO) && defined(LEPTON)
        // Save video files for both video and lepton*/
        if (result.count("video-lepton"))
        {
            cout << "Testing video and Lepton..." << endl;
            test_video_and_lepton();
        }
    #endif

    #if defined(LEPTON) && defined(DISPLAY)
        // Display Stream from Lepton and Save
        if (result.count("capture"))
        {
            cout << "Capturing stream..." << endl;
            capture_stream();
        }
    #endif

    return 0;
}

/*
 * Copyright (c) unknown
 *
 * SOURCE: https://github.com/natedomin/polyfit
 *
 * MODIFIED: Patrick Klein, 2018
 *
 */

#ifndef LANEGUIDANCE_POLYFIT_H_
#define LANEGUIDANCE_POLYFIT_H_

#include <array>
#include <vector>

#include <opencv2/core/core.hpp>

int polyfit(const std::vector<cv::Point2f>& points,
            std::array<float, 3>* coeffs);

#endif  // LANEGUIDANCE_POLYFIT_H_

/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Display.cpp                                                                       **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "Display.h"

#include <cstdio>
#include <iostream>

#include <opencv2/core/core.hpp>

#ifdef LEPTON
    #include "LeptonSPI/Stream.h"
#endif
#ifdef OLED
    #include "OLED/OLEDManager.h"
#endif

using cv::Mat;

#if defined(LEPTON) && defined(DISPLAY)
    void display_stream()
    {
        StreamObject stream;
        Mat gray;
        Mat color;
        stream.gray_image(gray);
        stream.color_image(color);

        Mat thresh;
        Mat big;
        namedWindow("FLIR Lepton", WINDOW_AUTOSIZE);

        for (::)
        {
            stream.next(gray);
            // threshold(gray, thresh, 128, 0, THRESH_TOZERO);
            // stream.colorize(gray, color);
            resize(gray, big, Size(0, 0), 4, 4);
            imshow("FLIR Lepton", big);
            waitKey(1);
        }
    }

    void capture_stream()
    {
        StreamObject stream;
        Mat gray;
        stream.gray_image(gray);

        char buffer[80];

        Mat big;
        namedWindow("FLIR Lepton", WINDOW_AUTOSIZE);

        for (int i = 0; i < 15 * FPS; i++)
        {
            printf("%i\n", i);

            stream.next(gray);
            snprintf(buffer, sizeof(buffer), "./Output/capture/capture_%u.png", clock());
            imwrite(buffer, gray);

            resize(gray, big, Size(0, 0), 4, 4);
            imshow("FLIR Lepton", big);
            waitKey(1);
        }
    }
#endif

#if defined(LEPTON) && defined(OLED)
    void display_OLED()
    {
        Mat gray, thresh, small;
        StreamObject stream;
        stream.gray_image(gray);
        OLEDManager oled;

        for (;;)
        {
            stream.next(gray);
            threshold(gray, thresh, 128, 1, THRESH_BINARY);
            resize(thresh, small, Size(OLED_WIDTH, OLED_HEIGHT));
            oled.display_image(small.data);
        }
    }
#endif

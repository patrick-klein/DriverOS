/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    LaneFinder.cpp                                                                    **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "LaneFinder.h"

#include <algorithm>
#include <array>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <vector>

#include "StackBuffer.h"
#include "polyfit.h"

using cv::COLOR_BGR2HLS;
using cv::Mat;
using cv::Point;
using cv::Point2f;
using cv::Range;
using cv::Scalar;
using std::array;
using std::for_each;
using std::min;
using std::shared_ptr;
using std::vector;

LaneFinder::LaneFinder(int rows, int cols)
    : _rows {rows}, _cols {cols}, _xm_per_pix {6.76f / _cols}, _ym_per_pix {30.0f / _rows}
{
    // Only calculate M and M_inv once for warp perspective
    Point2f srcPts[4] {
        Point2f {0.180f * cols, 0.972f * rows},
        Point2f {0.45f  * cols, 0.65f  * rows},
        Point2f {0.55f  * cols, 0.65f  * rows},
        Point2f {0.838f * cols, 0.972f * rows}
    };
    Point2f dstPts[4] {
        Point2f {0.305f * cols, static_cast<float>(rows)},
        Point2f {0.305f * cols, 0.0f},
        Point2f {0.695f * cols, 0.0f},
        Point2f {0.695f * cols, static_cast<float>(rows)}
    };
    _M = getPerspectiveTransform(srcPts, dstPts);
    _M_inv = getPerspectiveTransform(dstPts, srcPts);
}

void LaneFinder::find_lanes(const Mat& frame)
{
    // Find potential lane points in warped frame
    Mat warped, bin;
    _warp_image(frame, &warped);
    _threshold_image(warped, &bin);

    // Find nonzero points
    vector<Point> nonzero;
    _find_nonzero(bin, &nonzero);

    // Find initial points using histogram
    _find_base_with_histogram(bin);

    // Sliding windows to find points in left & right lane
    vector<Point2f> left_lane_points;
    vector<Point2f> right_lane_points;
    _sliding_windows(nonzero, &left_lane_points, &right_lane_points);

    // Do polyfit in pixels for each side
    _do_polytfit(LEFT_LANE, PIXELS, &left_lane_points);
    _do_polytfit(RIGHT_LANE, PIXELS, &right_lane_points);

    // Do polyfit in pixels for each side
    _do_polytfit(LEFT_LANE, METERS, &left_lane_points);
    _do_polytfit(RIGHT_LANE, METERS, &right_lane_points);

    _coeffs_valid = true;
}

void LaneFinder::get_lane_area(Mat* lane_area)
{
    vector<Point> points(2 * _rows);
    for (int i {0}; i < _rows; i++)
    {
        points[i] = Point(
            _left_coeffs_pixels[0] + _left_coeffs_pixels[1] * i + _left_coeffs_pixels[2] * i * i,
            i);
        points[2 * _rows - 1 - i] = Point(
            _right_coeffs_pixels[0] + _right_coeffs_pixels[1] * i + _right_coeffs_pixels[2] * i * i,
            i);
    }
    *lane_area = Mat::zeros(_rows, _cols, CV_8U);
    fillConvexPoly(*lane_area, points.data(), 2 * _rows, 1);
}

void LaneFinder::get_lane_lines(const lane_side_t side, Mat* lane_line)
{
    auto& coeffs_pixels {side == LEFT_LANE ? _left_coeffs_pixels : _right_coeffs_pixels};

    int line_width {7};
    int half {(line_width - 1) / 2};

    *lane_line = Mat::zeros(_rows, _cols, CV_8U);
    for (int i {0}; i < _rows; i++)
    {
        int j = coeffs_pixels[0] + coeffs_pixels[1] * i + coeffs_pixels[2] * i * i;
        for (int k {-half}; k < half + 1; k++)
        {
            if (j + k >= 0 && j + k < _cols)
            {
                lane_line->at<uint8_t>(i, j + k) = 1;
            }
        }
    }
}

float LaneFinder::get_position()
{
    float y_eval {_rows* _ym_per_pix};
    float leftBase {_left_coeffs_meters[2] *
                    powf(y_eval, 2) + _left_coeffs_meters[1] * y_eval + _left_coeffs_meters[0]};
    float rightBase {_right_coeffs_meters[2] *
                     powf(y_eval, 2) + _right_coeffs_meters[1] * y_eval + _right_coeffs_meters[0]};
    float center {(leftBase + rightBase) / 2};
    return (_xm_per_pix * _cols / 2) - center;
}

float LaneFinder::get_radius()
{
    float y_eval {_rows* _ym_per_pix};
    float leftRadius {powf(1 + powf(2 * _left_coeffs_meters[2] * y_eval + _left_coeffs_meters[1],
                                    2), 1.5f) / abs(2 * _left_coeffs_meters[2])};
    float rightRadius {powf(1 + powf(2 * _right_coeffs_meters[2] * y_eval + _right_coeffs_meters[1],
                                     2), 1.5f) / abs(2 * _right_coeffs_meters[2])};
    return (leftRadius + rightRadius) / 2;
}

void LaneFinder::unwarp_image(const Mat& src, Mat* dst) const
{
    warpPerspective(src, *dst, _M_inv, src.size());
}

void LaneFinder::_do_polytfit(const lane_side_t side,
                              const units_t units,
                              vector<Point2f>* lane_points_ptr)
{
    // Use member variables corresponding to side
    auto& coeffs {side == LEFT_LANE ?
                  (units == PIXELS ? _left_coeffs_pixels : _left_coeffs_meters) :
                  (units == PIXELS ? _right_coeffs_pixels : _right_coeffs_meters)};

    if (units == PIXELS)
    {
        auto& p_buffer {side == LEFT_LANE ? p_left_buffer : p_right_buffer};
        p_buffer->process(lane_points_ptr);
    }
    else
    {
        for_each(lane_points_ptr->begin(), lane_points_ptr->end(), [this](Point2f& p) {
            p.x *= _xm_per_pix;
            p.y *= _ym_per_pix;
        });
    }

    array<float, 3> temp;
    polyfit(*lane_points_ptr, &temp);

    bool coeffs_are_real {temp[0] == temp[0]
                          && temp[1] == temp[1]
                          && temp[2] == temp[2]};

    float alpha {0.9f};
    if (coeffs_are_real)
    {
        if (_coeffs_valid)
        {
            coeffs[0] = alpha * coeffs[0] + (1 - alpha) * temp[0];
            coeffs[1] = alpha * coeffs[1] + (1 - alpha) * temp[1];
            coeffs[2] = alpha * coeffs[2] + (1 - alpha) * temp[2];
        }
        else
        {
            coeffs[0] = temp[0];
            coeffs[1] = temp[1];
            coeffs[2] = temp[2];
        }
    }
}

void LaneFinder::_find_base_with_histogram(const Mat& bin)
{
    // Calculate histogram of bottom half of image
    Mat hist(1, _cols, CV_8U);
    for (int j {0}; j < _cols; j++)
    {
        hist.at<uint8_t>(0, j) = countNonZero(bin(Range(_rows / 2, _rows), Range(j, j + 1)));
    }

    // Find max values on left and right side
    double minVal, maxVal;
    Point minLoc, maxLocLeft, maxLocRight;
    minMaxLoc(hist(Range::all(), Range(0, _cols / 2)), &minVal, &maxVal, &minLoc, &maxLocLeft);
    minMaxLoc(hist(Range::all(), Range(_cols / 2, _cols)), &minVal, &maxVal, &minLoc, &maxLocRight);
    _left_base = maxLocLeft.x;
    _right_base = maxLocRight.x + _cols / 2;
}

void LaneFinder::_find_nonzero(const Mat& bin, vector<Point>* nonzero)
{
    for (int i {0}; i < _rows; i++)
    {
        for (int j {0}; j < _cols; j++)
        {
            if (bin.at<uint8_t>(i, j))
            {
                nonzero->push_back(Point(j, i));
            }
        }
    }
}

void LaneFinder::_sliding_windows(const vector<Point> nonzero,
                                  vector<Point2f>* left_lane_points_ptr,
                                  vector<Point2f>* right_lane_points_ptr)
{
    int leftIndex {_left_base};
    int rightIndex {_right_base};
    int nWindows {9};
    int windowHeight {_rows / nWindows};
    int margin {_cols / 11};
    int minpix {10};

    for (int i {0}; i < nWindows; i++)
    {
        int win_y_low       {_rows - (i + 1) * windowHeight};
        int win_y_high      {_rows - i * windowHeight};
        int win_xleft_low   {leftIndex - margin};
        int win_xleft_high  {leftIndex + margin};
        int win_xright_low  {rightIndex - margin};
        int win_xright_high {rightIndex + margin};

        vector<Point> newLeft, newRight;
        for (auto const& p : nonzero)
        {
            if (p.y >= win_y_low && p.y < win_y_high
                && p.x >= win_xleft_low && p.x < win_xleft_high)
            {
                newLeft.push_back(p);
            }
            else if (p.y >= win_y_low && p.y < win_y_high
                     && p.x >= win_xright_low && p.x < win_xright_high)
            {
                newRight.push_back(p);
            }
        }
        left_lane_points_ptr->insert(left_lane_points_ptr->end(),
                                     newLeft.begin(), newLeft.end());
        right_lane_points_ptr->insert(right_lane_points_ptr->end(),
                                      newRight.begin(), newRight.end());
        if (newLeft.size() >= minpix)
        {
            int leftXSum {0};
            for (auto const& p : newLeft)
            {
                leftXSum += p.x;
            }
            leftIndex = leftXSum / newLeft.size();
            if (i == 0)
            {
                _left_base = leftIndex;
            }
        }
        if (newRight.size() >= minpix)
        {
            int rightXSum {0};
            for (auto const& p : newRight)
            {
                rightXSum += p.x;
            }
            rightIndex = rightXSum / newRight.size();
            if (i == 0)
            {
                _right_base = rightIndex;
            }
        }
    }
}

void LaneFinder::_threshold_image(const Mat& src, Mat* dst) const
{
    Mat image_hls;
    cvtColor(src, image_hls, COLOR_BGR2HLS);
    Mat temp;

    // Yellow lane
    inRange(src, Scalar {0, 0, 140}, Scalar {170, 255, 255}, *dst);
    inRange(image_hls, Scalar {0, 100, 80}, Scalar {50, 220, 255}, temp);
    *dst &= temp;

    // White lane
    inRange(image_hls, Scalar {0, 200, 0}, Scalar {180, 255, 255}, temp);
    *dst |= temp;
}

void LaneFinder::_warp_image(const Mat& src, Mat* dst) const
{
    warpPerspective(src, *dst, _M, src.size());
}

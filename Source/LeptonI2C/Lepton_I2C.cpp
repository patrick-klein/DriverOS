/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Lepton_I2C.cpp                                                                    **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "Lepton_I2C.h"

#include "LEPTON_AGC.h"
#include "LEPTON_OEM.h"
#include "LEPTON_SDK.h"
#include "LEPTON_SYS.h"
#include "LEPTON_Types.h"

bool _connected;

LEP_CAMERA_PORT_DESC_T _port;

int lepton_connect()
{
    LEP_OpenPort(1, LEP_CCI_TWI, 400, &_port);
    _connected = true;
    return 0;
}

void lepton_perform_ffc()
{
    if (!_connected)
        lepton_connect();
    LEP_RunSysFFCNormalization(&_port);
}

int lepton_get_agc_state()
{
    if (!_connected)
        lepton_connect();
    LEP_AGC_ENABLE_E state;
    LEP_AGC_ENABLE_E_PTR state_ptr = &state;
    LEP_GetAgcEnableState(&_port, state_ptr);
    return (int) state;
}

void lepton_set_agc_state(int state)
{
    if (!_connected)
        lepton_connect();
    if (state)
        LEP_SetAgcEnableState(&_port, LEP_AGC_ENABLE);
    else
        LEP_SetAgcEnableState(&_port, LEP_AGC_DISABLE);
}


void lepton_do_reboot()
{
    if (!_connected)
        lepton_connect();
    LEP_RunOemReboot(&_port);
}

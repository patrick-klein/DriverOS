/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    StackBuffer.h                                                                     **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#ifndef LANEGUIDANCE_STACKBUFFER_H_
#define LANEGUIDANCE_STACKBUFFER_H_

#include <vector>

template <class T>
class StackBuffer
{
 public:
    explicit StackBuffer(int size) : _max_size {size}, _buffer(_max_size) {}

    int size();
    void process(std::vector<T>* vector_ptr);
    void read_from_buffer(std::vector<T>* dst);
    void write_to_buffer(const std::vector<T>& src, int num_to_write);

 private:
    int _max_size;
    std::vector<T> _buffer;
    int _size {0};
    int _right_index {0};
    int _write_index {0};
};

template <class T>
int StackBuffer<T>::size()
{
    return _size;
}

template <class T>
void StackBuffer<T>::process(std::vector<T>* vector_ptr)
{
    int num_points = vector_ptr->size();
    read_from_buffer(vector_ptr);
    write_to_buffer(*vector_ptr, num_points);
}

template <class T>
void StackBuffer<T>::read_from_buffer(std::vector<T>* dst)
{
    for (int i {0}; i < _size; i++)
    {
        if (_right_index <= 0)
        {
            _right_index = _size - 1;
        }
        dst->push_back(_buffer[_right_index--]);
    }
}

template <class T>
void StackBuffer<T>::write_to_buffer(const std::vector<T>& src, int num_to_write)
{
    for (int i {0}; i < num_to_write; i++)
    {
        if (_write_index == _max_size)
        {
            _write_index = 0;
            _size = _max_size;
        }
        _buffer[_write_index++] = src[i];
    }
    if (_write_index > _size)
    {
        _size = _write_index;
    }
    _right_index = _write_index - 1;
}

#endif  // LANEGUIDANCE_STACKBUFFER_H_

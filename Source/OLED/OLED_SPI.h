/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * MODIFIED: Patrick Klein, 2018
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 */

#ifndef OLED_OLED_SPI_H_
#define OLED_OLED_SPI_H_

namespace SPI2 {
extern int spi_fd;
extern unsigned char spi_mode;
extern unsigned char spi_bits_per_word;
extern unsigned int spi_speed;

int SPI_open_port();
int SPI_close_port();
}  // namespace SPI2

#endif  // OLED_OLED_SPI_H_

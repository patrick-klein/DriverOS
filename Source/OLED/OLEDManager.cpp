/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    Test.cpp                                                                          **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "OLEDManager.h"

#include <sys/time.h>
#include <cstdio>

#include "GPIOConst.h"
#include "OLED_SPI.h"

using namespace GPIO;
using namespace SPI2;

OLEDManager::OLEDManager()
{
    // Open /dev/spidev2.0
    SPI_open_port();

    // Initialize GPIOManager and export pins
    gp = GPIOManager::get_instance();
    rst = GPIOConst::get_instance()->getGpioByKey(OLED_RST);
    dc = GPIOConst::get_instance()->getGpioByKey(OLED_DC);
    gp->export_pin(rst);
    gp->export_pin(dc);

    // Set pins to output
    gp->set_direction(rst, OUTPUT);
    gp->set_direction(dc, OUTPUT);

    // Cycle rst values for power-on cycle
    gp->set_value(rst, LOW);
    usleep(100);
    gp->set_value(rst, HIGH);

    // Display Init sequence for 64x48 OLED module
    unsigned char comm_buf[23];

    comm_buf[0] = DISPLAYOFF;
    comm_buf[1] = SETDISPLAYCLOCKDIV;
    comm_buf[2] = 0x80;
    comm_buf[3] = SETMULTIPLEX;
    comm_buf[4] = 0x2F;
    comm_buf[5] = SETDISPLAYOFFSET;
    comm_buf[6] = 0x0;
    comm_buf[7] = SETSTARTLINE | 0x0;
    comm_buf[8] = CHARGEPUMP;
    comm_buf[9] = 0x14;
    comm_buf[10] = NORMALDISPLAY;
    comm_buf[11] = DISPLAYALLONRESUME;
    comm_buf[12] = SEGREMAP | 0x1;
    comm_buf[13] = COMSCANDEC;
    comm_buf[14] = SETCOMPINS;
    comm_buf[15] = 0x12;
    comm_buf[16] = SETCONTRAST;
    comm_buf[17] = 0x8F;
    comm_buf[18] = SETPRECHARGE;
    comm_buf[19] = 0xF1;
    comm_buf[20] = SETVCOMDESELECT;
    comm_buf[21] = 0x40;
    comm_buf[22] = DISPLAYON;

    send_command(comm_buf, 23);

    // Erase hardware memory inside the OLED controller to avoid random data in memory.
    clear_display();

    // char comm = DISPLAYALLON;
    // send_command(&comm, 1);

    // test_image();
}

OLEDManager::~OLEDManager()
{
    SPI_close_port();
    gp->~GPIOManager();
}

int OLEDManager::clear_display()
{
    unsigned char empty_buf[0x80];
    memset(empty_buf, 0, 0x80);

    for (char p = 0; p < 8; p++)
    {
        set_page_address(p);
        set_column_address(0);
        send_data(empty_buf, 0x80);
    }

    return 0;
}

int OLEDManager::display_image(oled_image image)
{
    oled_buf buffer;

    for (int i = 0; i < OLED_HEIGHT; i++)
    {
        for (int j = 0; j < OLED_WIDTH; j++)
        {
            if (i % 8 == 0)
            {
                buffer[(i / 8) * OLED_WIDTH + j] = 0;
            }
            buffer[i / 8 * OLED_WIDTH + j] |= image[i * OLED_WIDTH + j] << (i % 8);
        }
    }

    for (char p = 0; p < 6; p++)
    {
        set_page_address(p);
        set_column_address(0);
        send_data(buffer + p * 0x40, 0x40);
    }

    return 0;
}

int OLEDManager::send_command(const unsigned char comm_buf[], const int num_bytes_to_send)
{
    gp->set_value(dc, LOW);
    write(spi_fd, comm_buf, num_bytes_to_send);
    return 0;
}

int OLEDManager::send_data(const unsigned char data_buf[], const int num_bytes_to_send)
{
    gp->set_value(dc, HIGH);
    write(spi_fd, data_buf, num_bytes_to_send);
    return 0;
}

int OLEDManager::set_page_address(unsigned char addr)
{
    addr |= 0xB0;
    send_command(&addr, 1);
    return 0;
}

int OLEDManager::set_column_address(unsigned char addr)
{
    unsigned char comm_buf[2];
    comm_buf[0] = (0x10 | (addr >> 4)) + 0x02;
    comm_buf[1] = addr & 0x0F;
    send_command(comm_buf, 2);
    return 0;
}

int OLEDManager::test_image()
{
    static unsigned char screenmemory[] = {
        /* LCD Memory organised in 64 horizontal pixel and 6 rows of byte
           B  B .............B  -----
           y  y .............y        \
           t  t .............t         \
           e  e .............e          \
           0  1 .............63          \
           //                             \
           D0 D0.............D0            \
           D1 D1.............D1            / ROW 0
           D2 D2.............D2           /
           D3 D3.............D3          /
           D4 D4.............D4         /
           D5 D5.............D5        /
           D6 D6.............D6       /
           D7 D7.............D7  ----
         */
        // SparkFun Electronics LOGO

        // ROW0, BYTE0 to BYTE63
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0xF8, 0xFC, 0xFE, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x0F, 0x07, 0x07, 0x06, 0x06, 0x00, 0x80, 0x80, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        // ROW1, BYTE64 to BYTE127
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x81, 0x07, 0x0F, 0x3F, 0x3F, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFE, 0xFC, 0xFC, 0xFC, 0xFE, 0xFF, 0xFF, 0xFF,
        0xFC, 0xF8, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        // ROW2, BYTE128 to BYTE191
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xFC, 0xFE, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xF1, 0xE0, 0xE0, 0xE0, 0xE0, 0xE0, 0xF0,
        0xFD, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        // ROW3, BYTE192 to BYTE255
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x3F,
        0x1F, 0x07, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        // ROW4, BYTE256 to BYTE319
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F, 0x3F, 0x1F, 0x1F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x07, 0x07, 0x07, 0x03, 0x03, 0x01, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,

        // ROW5, BYTE320 to BYTE383
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0xFF, 0x7F, 0x3F, 0x1F, 0x0F, 0x07, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00
    };

    for (char i = 0; i < 6; i++)
    {
        set_page_address(i);
        set_column_address(0);
        send_data(screenmemory + i * 0x40, 0x40);
    }

    return 0;
}

/*
 * Copyright (c) 2013 Mateusz Kaczanowski
 * www.mkaczanowski.com
 *
 * MODIFIED: Patrick Klein, 2018
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:

 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.

 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef OLED_GPIOMANAGER_H_
#define OLED_GPIOMANAGER_H_

#define SYSFS_GPIO_DIR "/sys/class/gpio"

#include <fcntl.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <algorithm>
#include <fstream>
#include <vector>

namespace GPIO {
enum DIRECTION
{
    INPUT = 0,
    OUTPUT = 1
};

enum PIN_VALUE
{
    LOW = 0,
    HIGH = 1
};

enum EDGE_VALUE
{
    NONE = 0,
    RISING = 1,
    FALLING = 2,
    BOTH = 3
};

class GPIOManager
{
 public:
    GPIOManager();
    virtual ~GPIOManager();

    static GPIOManager* get_instance();
    int export_pin(unsigned int gpio);
    int unexport_pin(unsigned int gpio);
    int set_direction(unsigned int gpio, DIRECTION direction);
    int get_direction(unsigned int gpio);
    int set_value(unsigned int gpio, PIN_VALUE value);
    int get_value(unsigned int gpio);
    int set_edge(unsigned int gpio, EDGE_VALUE value);
    int get_edge(unsigned int gpio);
    int wait_for_edge(unsigned int gpio, EDGE_VALUE value);
    int count_exported_pins();
    void clean();
 private:
    static GPIOManager* instance;
    std::vector<unsigned int> exported_pins;
};
}  // namespace GPIO
#endif  // OLED_GPIOMANAGER_H_

/*****************************************************************************************
**                                                                                      **
**    Copyright (c) 2018, Patrick Klein                                                 **
**                                                                                      **
**    DriverOS                                                                          **
**    yolo.cpp                                                                          **
**                                                                                      **
**    Permission is hereby granted, free of charge, to any person obtaining a copy      **
**    of this software and associated documentation files (the "Software"), to deal     **
**    in the Software without restriction, including without limitation the rights      **
**    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell         **
**    copies of the Software, and to permit persons to whom the Software is             **
**    furnished to do so, subject to the following conditions:                          **
**                                                                                      **
**    The above copyright notice and this permission notice shall be included in all    **
**    copies or substantial portions of the Software.                                   **
**                                                                                      **
**    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR        **
**    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,          **
**    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE       **
**    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER            **
**    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,     **
**    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     **
**    SOFTWARE.                                                                         **
*****************************************************************************************/

#include "yolo.h"

#include <algorithm>
#include <string>
#include <vector>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using caffe::Blob;
using caffe::Caffe;
using caffe::Net;
using caffe::TEST;
using cv::COLOR_BGRA2RGB;
using cv::COLOR_BGR2RGB;
using cv::COLOR_GRAY2RGB;
using cv::Mat;
using cv::Rect;
using google::InitGoogleLogging;
using google::SetCommandLineOption;
using std::min;
using std::string;
using std::vector;

YOLODetector::YOLODetector()
{
    InitGoogleLogging("DriverOS");
    SetCommandLineOption("GLOG_minloglevel", "2");

    Caffe::set_mode(Caffe::GPU);

    string model_filename {"Model/yolo_small_deploy.prototxt"};
    string weight_filename {"Model/yolo_small.caffemodel"};

    _net.reset(new Net<float>(model_filename, TEST));
    _net->CopyTrainedLayersFrom(weight_filename);
}

void YOLODetector::detect(Mat* img,
                          vector<float>* probs,
                          vector<vector<float>>* boxes,
                          vector<string>* classes)
{
    // Pre-process image
    _preprocess(img);

    // Get network output from image
    vector<float> output {_forwardPass(img)};

    // Get results from net output
    probs->clear();
    boxes->clear();
    classes->clear();
    _parse_output(&output, probs, boxes, classes);

    // TODO(12): post process to make boxes align with original image
}

vector<float> YOLODetector::_forwardPass(Mat* img)
{
    Blob<float>* input_layer {_net->input_blobs()[0]};
    input_layer->Reshape(1, 3, HEIGHT, WIDTH);

    // Forward dimension change to all layers.
    _net->Reshape();

    // Wrap input layer
    vector<Mat> input_channels;
    int width {input_layer->width()};
    int height {input_layer->height()};
    float* input_data {input_layer->mutable_cpu_data()};
    for (int i {0}; i < input_layer->channels(); ++i)
    {
        Mat channel(height, width, CV_32FC1, input_data);
        input_channels.push_back(channel);
        input_data += width * height;
    }

    // Forward pass
    split(*img, input_channels);
    _net->Forward();

    Blob<float>* output_layer {_net->output_blobs()[0]};
    const float* begin {output_layer->cpu_data()};
    const float* end {begin + output_layer->channels()};

    vector<float> output(begin, end);
    return output;
}

void YOLODetector::_preprocess(Mat* img)
{
    // Convert the input image to the input image format of the network.
    if (img->channels() == 4)
        cvtColor(*img, *img, COLOR_BGRA2RGB);
    else if (img->channels() == 1)
        cvtColor(*img, *img, COLOR_GRAY2RGB);
    else
        cvtColor(*img, *img, COLOR_BGR2RGB);

    // Pad and resize image
    int width {img->cols};
    int height {img->rows};
    Mat square {Mat::zeros(WIDTH, WIDTH, img->type())};
    int max_dim {(width >= height) ? width : height};
    float scale {(static_cast<float>(WIDTH)) / max_dim};
    Rect roi;
    if (width >= height)
    {
        roi.width = WIDTH;
        roi.x = 0;
        roi.height = height * scale;
        roi.y = (WIDTH - roi.height) / 2;
    }
    else
    {
        roi.y = 0;
        roi.height = HEIGHT;
        roi.width = width * scale;
        roi.x = (WIDTH - roi.width) / 2;
    }
    resize(*img, square(roi), roi.size());
    *img = square;

    // Convert to float and scale
    img->convertTo(*img, CV_32FC3, 1. / 255);
}

void YOLODetector::_parse_output(vector<float>* output,
                                 vector<float>* probs,
                                 vector<vector<float>>* boxes,
                                 vector<string>* classes)
{
    vector<int> class_nums;
    vector<float> box;
    for (int x {0}; x < GRID_SIZE; x++)
    {
        for (int y {0}; y < GRID_SIZE; y++)
        {
            for (int i {0}; i < NUM_BOXES; i++)
            {
                box.clear();
                for (int j {0}; j < N_COORD_BOX; j++)
                {
                    float v {output->data()[BOX_INDEX(x, y, i, j) + BOX_OFFSET]};
                    if (j == 0)
                        box.push_back((WIDTH / GRID_SIZE) * (v + y));
                    else if (j == 1)
                        box.push_back((HEIGHT / GRID_SIZE) * (v + x));
                    else if (j == 2)
                        box.push_back(WIDTH * v * v);
                    else if (j == 3)
                        box.push_back(HEIGHT * v * v);
                }
                for (int j {0}; j < NUM_CLASSES; j++)
                {
                    float p {output->data()[CLS_INDEX(x, y, j)] *
                             output->data()[CON_INDEX(x, y, i) + CON_OFFSET]};
                    if (p > threshold)
                    {
                        probs->push_back(p);
                        class_nums.push_back(j);
                        boxes->push_back(box);
                    }
                }
            }
        }
    }

    _non_maxima_suppression(probs, boxes, &class_nums);

    for (auto const& e : class_nums)
        classes->push_back(class_arr[e]);
}

void YOLODetector::_non_maxima_suppression(vector<float>* probs,
                                           vector<vector<float>>* boxes,
                                           vector<int>* class_nums)
{
    for (int i {0}; i < probs->size(); i++)
    {
        for (int j {i + 1}; j < probs->size(); j++)
        {
            if ((*class_nums)[i] == (*class_nums)[j]
                && _iou((*boxes)[i], (*boxes)[j]) > iou_threshold)
            {
                probs->erase(probs->begin() + j);
                class_nums->erase(class_nums->begin() + j);
                boxes->erase(boxes->begin() + j);
                j--;
            }
        }
    }
}

float YOLODetector::_iou(const vector<float>& box1, const vector<float>& box2)
{
    float int_tb {std::min(box1[0] + 0.5f * box1[2], box2[0] + 0.5f * box2[2])
                  - std::max(box1[0] - 0.5f * box1[2], box2[0] - 0.5f * box2[2])};
    float int_lr {std::min(box1[1] + 0.5f * box1[3], box2[1] + 0.5f * box2[3])
                  - std::max(box1[1] - 0.5f * box1[3], box2[1] - 0.5f * box2[3])};
    float intersection {std::max(0.0f, int_tb) * std::max(0.0f, int_lr)};
    float area1 {box1[2] * box1[3]};
    float area2 {box2[2] * box2[3]};
    float control_area {std::min(area1, area2)};
    return intersection / control_area;
}
